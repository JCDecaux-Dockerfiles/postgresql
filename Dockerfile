FROM jcdecaux/rhel:7
MAINTAINER Nicolas Mallet <nicolas.mallet@jcdecaux.com>

ENV LANG='en_US.UTF-8' LANGUAGE='en_US:en' LC_ALL='en_US.UTF-8'

COPY pgdg-redhat96-9.6-3.noarch.rpm /tmp/pgdg-redhat96-9.6-3.noarch.rpm
RUN rpm -Uvh /tmp/pgdg-redhat96-9.6-3.noarch.rpm

COPY yum.conf /etc/yum.conf

RUN yum install -y supervisor postgresql96-server postgresql96-contrib postgresql96-pltcl \
  && yum clean all

COPY postgresql96-setup /usr/pgsql-9.6/bin/postgresql96-setup
RUN /usr/pgsql-9.6/bin/postgresql96-setup initdb

COPY pg_hba.conf /var/lib/pgsql/9.6/data/pg_hba.conf
RUN chown postgres:postgres /var/lib/pgsql/9.6/data/pg_hba.conf

COPY postgresql.conf /var/lib/pgsql/9.6/data/postgresql.conf
RUN chown postgres:postgres /var/lib/pgsql/9.6/data/postgresql.conf

COPY supervisord.conf /etc/supervisord.conf

VOLUME ["/var/lib/pgsql"]

EXPOSE 5432

CMD ["/usr/bin/supervisord"]
